<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

    <title>Room</title>
</head>
<body class="container">
<div class="pull-right">
    <p>
        <a href="/home/add" class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> Add</a>
    </p>
</div>

<table class="table table-striped" border="2">
    <thead>
    <th>room_id</th>
    <th>roomType</th>
    <th>price</th>
    <th>description</th>
    <th>location</th>
    <th>facilities</th>
    </thead>
    <c:forEach items="${roomlist}" var="c">
        <tr>
            <td>${c.room_id}</td>
            <td>${c.roomType}</td>
            <td>${c.price}</td>
            <td>${c.description}</td>
            <td>${c.location}</td>
            <td>${c.facilities}</td>
            <td>
                <a href="/home/update?id=<c:out value="${c.id}"/>" class="btn btn-success"><span class="glyphicon glyphicon-pencil"></span> </a>
                <a href="/home/delete?id=<c:out value="${c.id}"/>" class="btn btn-danger" onclick="return confirm('Are you Sure?')"><span class="glyphicon glyphicon-trash"></span> </a>
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
